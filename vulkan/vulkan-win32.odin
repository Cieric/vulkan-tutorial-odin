package vk

foreign import vulkan "vulkan-1.lib"

import win32 "core:sys/win32"
import _c "core:c"

VULKAN_WIN32_H_ :: 1;
KHR_WIN32_SURFACE :: 1;
KHR_WIN32_SURFACE_SPEC_VERSION :: 6;
KHR_WIN32_SURFACE_EXTENSION_NAME :: "VK_KHR_win32_surface";
KHR_EXTERNAL_MEMORY_WIN32 :: 1;
KHR_EXTERNAL_MEMORY_WIN32_SPEC_VERSION :: 1;
KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME :: "VK_KHR_external_memory_win32";
KHR_WIN32_KEYED_MUTEX :: 1;
KHR_WIN32_KEYED_MUTEX_SPEC_VERSION :: 1;
KHR_WIN32_KEYED_MUTEX_EXTENSION_NAME :: "VK_KHR_win32_keyed_mutex";
KHR_EXTERNAL_SEMAPHORE_WIN32 :: 1;
KHR_EXTERNAL_SEMAPHORE_WIN32_SPEC_VERSION :: 1;
KHR_EXTERNAL_SEMAPHORE_WIN32_EXTENSION_NAME :: "VK_KHR_external_semaphore_win32";
KHR_EXTERNAL_FENCE_WIN32 :: 1;
KHR_EXTERNAL_FENCE_WIN32_SPEC_VERSION :: 1;
KHR_EXTERNAL_FENCE_WIN32_EXTENSION_NAME :: "VK_KHR_external_fence_win32";
NV_EXTERNAL_MEMORY_WIN32 :: 1;
NV_EXTERNAL_MEMORY_WIN32_SPEC_VERSION :: 1;
NV_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME :: "VK_NV_external_memory_win32";
NV_WIN32_KEYED_MUTEX :: 1;
NV_WIN32_KEYED_MUTEX_SPEC_VERSION :: 1;
NV_WIN32_KEYED_MUTEX_EXTENSION_NAME :: "VK_NV_win32_keyed_mutex";

Win32SurfaceCreateFlagsKHR :: Flags;
PFN_CreateWin32SurfaceKHR :: #type proc();
PFN_GetPhysicalDeviceWin32PresentationSupportKHR :: #type proc();
PFN_GetMemoryWin32HandleKHR :: #type proc();
PFN_GetMemoryWin32HandlePropertiesKHR :: #type proc();
PFN_ImportSemaphoreWin32HandleKHR :: #type proc();
PFN_GetSemaphoreWin32HandleKHR :: #type proc();
PFN_ImportFenceWin32HandleKHR :: #type proc();
PFN_GetFenceWin32HandleKHR :: #type proc();
PFN_GetMemoryWin32HandleNV :: #type proc();

Win32SurfaceCreateInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    flags : Win32SurfaceCreateFlagsKHR,
    hinstance : win32.Hinstance,
    hwnd : win32.Hwnd,
};

ImportMemoryWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    handleType : ExternalMemoryHandleTypeFlagBits,
    handle : win32.Handle,
    name : win32.Lpcwstr,
};

ExportMemoryWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    pAttributes : ^win32.Security_Attributes,
    dwAccess : win32.Dword,
    name : win32.Lpcwstr,
};

MemoryWin32HandlePropertiesKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    memoryTypeBits : u32,
};

MemoryGetWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    memory : DeviceMemory,
    handleType : ExternalMemoryHandleTypeFlagBits,
};

Win32KeyedMutexAcquireReleaseInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    acquireCount : u32,
    pAcquireSyncs : ^DeviceMemory,
    pAcquireKeys : ^u64,
    pAcquireTimeouts : ^u32,
    releaseCount : u32,
    pReleaseSyncs : ^DeviceMemory,
    pReleaseKeys : ^u64,
};

ImportSemaphoreWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    semaphore : Semaphore,
    flags : SemaphoreImportFlags,
    handleType : ExternalSemaphoreHandleTypeFlagBits,
    handle : win32.Handle,
    name : win32.Lpcwstr,
};

ExportSemaphoreWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    pAttributes : ^win32.Security_Attributes,
    dwAccess : win32.Dword,
    name : win32.Lpcwstr,
};

D3D12FenceSubmitInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    waitSemaphoreValuesCount : u32,
    pWaitSemaphoreValues : ^u64,
    signalSemaphoreValuesCount : u32,
    pSignalSemaphoreValues : ^u64,
};

SemaphoreGetWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    semaphore : Semaphore,
    handleType : ExternalSemaphoreHandleTypeFlagBits,
};

ImportFenceWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    fence : Fence,
    flags : FenceImportFlags,
    handleType : ExternalFenceHandleTypeFlagBits,
    handle : win32.Handle,
    name : win32.Lpcwstr,
};

ExportFenceWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    pAttributes : ^win32.Security_Attributes,
    dwAccess : win32.Dword,
    name : win32.Lpcwstr,
};

FenceGetWin32HandleInfoKHR :: struct {
    sType : StructureType,
    pNext : rawptr,
    fence : Fence,
    handleType : ExternalFenceHandleTypeFlagBits,
};

ImportMemoryWin32HandleInfoNV :: struct {
    sType : StructureType,
    pNext : rawptr,
    handleType : ExternalMemoryHandleTypeFlagsNV,
    handle : win32.Handle,
};

ExportMemoryWin32HandleInfoNV :: struct {
    sType : StructureType,
    pNext : rawptr,
    pAttributes : ^win32.Security_Attributes,
    dwAccess : win32.Dword,
};

Win32KeyedMutexAcquireReleaseInfoNV :: struct {
    sType : StructureType,
    pNext : rawptr,
    acquireCount : u32,
    pAcquireSyncs : ^DeviceMemory,
    pAcquireKeys : ^u64,
    pAcquireTimeoutMilliseconds : ^u32,
    releaseCount : u32,
    pReleaseSyncs : ^DeviceMemory,
    pReleaseKeys : ^u64,
};

@(default_calling_convention="c")
foreign vulkan {

    @(link_name="vkCreateWin32SurfaceKHR")
    create_win32_surface_khr :: proc(
        instance : Instance,
        pCreateInfo : ^Win32SurfaceCreateInfoKHR,
        pAllocator : ^AllocationCallbacks,
        pSurface : ^SurfaceKHR
    ) -> Result ---;

    @(link_name="vkGetPhysicalDeviceWin32PresentationSupportKHR")
    get_physical_device_win32_presentation_support_khr :: proc(
        physicalDevice : PhysicalDevice,
        queueFamilyIndex : u32
    ) -> Bool32 ---;

    @(link_name="vkGetMemoryWin32HandleKHR")
    get_memory_win32_handle_khr :: proc(
        device : Device,
        pGetWin32HandleInfo : ^MemoryGetWin32HandleInfoKHR,
        pHandle : ^win32.Handle
    ) -> Result ---;

    @(link_name="vkGetMemoryWin32HandlePropertiesKHR")
    get_memory_win32_handle_properties_khr :: proc(
        device : Device,
        handleType : ExternalMemoryHandleTypeFlagBits,
        handle : win32.Handle,
        pMemoryWin32HandleProperties : ^MemoryWin32HandlePropertiesKHR
    ) -> Result ---;

    @(link_name="vkImportSemaphoreWin32HandleKHR")
    import_semaphore_win32_handle_khr :: proc(
        device : Device,
        pImportSemaphoreWin32HandleInfo : ^ImportSemaphoreWin32HandleInfoKHR
    ) -> Result ---;

    @(link_name="vkGetSemaphoreWin32HandleKHR")
    get_semaphore_win32_handle_khr :: proc(
        device : Device,
        pGetWin32HandleInfo : ^SemaphoreGetWin32HandleInfoKHR,
        pHandle : ^win32.Handle
    ) -> Result ---;

    @(link_name="vkImportFenceWin32HandleKHR")
    import_fence_win32_handle_khr :: proc(
        device : Device,
        pImportFenceWin32HandleInfo : ^ImportFenceWin32HandleInfoKHR
    ) -> Result ---;

    @(link_name="vkGetFenceWin32HandleKHR")
    get_fence_win32_handle_khr :: proc(
        device : Device,
        pGetWin32HandleInfo : ^FenceGetWin32HandleInfoKHR,
        pHandle : ^win32.Handle
    ) -> Result ---;

    @(link_name="vkGetMemoryWin32HandleNV")
    get_memory_win32_handle_nv :: proc(
        device : Device,
        memory : DeviceMemory,
        handleType : ExternalMemoryHandleTypeFlagsNV,
        pHandle : ^win32.Handle
    ) -> Result ---;

}
