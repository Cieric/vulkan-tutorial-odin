package window_manager;

import win32 "core:sys/win32"
import fmt "core:fmt"

ErrorStr :: cstring;

Window :: struct {
    hInstance : win32.Hinstance,
    hwnd : win32.Hwnd,
    width : u32,
    height : u32,
};

WndProc :: proc "c" (hwnd : win32.Hwnd, uMsg : u32, wParam : win32.Wparam, lParam : win32.Lparam) -> win32.Lresult
{
    switch (uMsg)
	{
		case win32.WM_DESTROY:
        {
			win32.post_quit_message(0);
			return 0;
        }
		case win32.WM_PAINT:
		{
			ps : win32.Paint_Struct = {};
			hdc : win32.Hdc = win32.begin_paint(hwnd, &ps);

			win32.fill_rect(hdc, &ps.rcPaint, win32.COLOR_BACKGROUND);

			win32.end_paint(hwnd, &ps);
			return 0;
		}
	}
	return win32.def_window_proc_a(hwnd, uMsg, wParam, lParam);
}

spawn_window :: proc(windowName : cstring, width : u32 = 640, height : u32 = 480 ) -> (ErrorStr, ^Window) {

	// Register the window class.
    CLASS_NAME : cstring = "Main Vulkan Window";

	wc : win32.Wnd_Class_Ex_A = {}; 

    hInstance := transmute(win32.Hinstance)(win32.get_module_handle_a(nil));

    wc.size = size_of(win32.Wnd_Class_Ex_A);
	wc.wnd_proc = WndProc;
	wc.instance = hInstance;
	wc.class_name = CLASS_NAME;

	if win32.register_class_ex_a(&wc) == 0 do return "Failed to register class!", nil;

    hwnd := win32.create_window_ex_a(
        0,
        CLASS_NAME,
        windowName,
        win32.WS_OVERLAPPEDWINDOW | win32.WS_VISIBLE,

        win32.CW_USEDEFAULT, win32.CW_USEDEFAULT, 640, 480,
        
        nil,
        nil,
        hInstance,
        nil,
    );

    if hwnd == nil do return "failed to create window!", nil;

    window := new(Window);
    window.hInstance = hInstance;
    window.hwnd = hwnd;
    window.width = width;
    window.height = height;

    return nil, window;
}

handle_msgs :: proc(window : ^Window) -> bool
{
    msg : win32.Msg = {};
    cont : bool = true;
    for win32.peek_message_a(&msg, nil, 0, 0, win32.PM_REMOVE)
    { 
        if msg.message == win32.WM_QUIT do cont = false;
        win32.translate_message(&msg);
        win32.dispatch_message_a(&msg);
    }
    return cont;
}