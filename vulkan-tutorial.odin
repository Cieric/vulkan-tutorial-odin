package vulkan_tutorial;

import vk "vulkan"
import wm "window_manager"
import mem "core:mem"
import os "core:os"
import bits "core:math/bits"

WIDTH : u32 = 800;
HEIGHT : u32 = 600;
MAX_FRAMES_IN_FLIGHT : u32 = 2;

validationLayers : []cstring = {
    "VK_LAYER_KHRONOS_validation",
};

deviceExtensions : []cstring = {
    vk.KHR_SWAPCHAIN_EXTENSION_NAME,
};

when ODIN_DEBUG {
    enableValidationLayers : bool = true;
} else {
    enableValidationLayers : bool = false;
}

QueueFamilyIndices :: struct {
    graphicsFamily : u32,
    presentFamily : u32,
};

QueueFamilyIndices_isComplete :: proc(this : ^QueueFamilyIndices) -> bool {
    return (this.graphicsFamily != ~cast(u32)(0)) && (this.presentFamily != ~cast(u32)(0));
}

SwapChainSupportDetails :: struct {
    capabilities : vk.SurfaceCapabilitiesKHR,
    formats : []vk.SurfaceFormatKHR,
    presentModes : []vk.PresentModeKHR,
};

window : ^wm.Window;

instance : vk.Instance;
surface : vk.SurfaceKHR;

physicalDevice : vk.PhysicalDevice = nil;
device : vk.Device;

graphicsQueue : vk.Queue;
presentQueue : vk.Queue;

swapChain : vk.SwapchainKHR;
swapChainImages : []vk.Image;
swapChainImageFormat : vk.Format;
swapChainExtent : vk.Extent2D;
swapChainImageViews : []vk.ImageView;
swapChainFramebuffers : []vk.Framebuffer;

renderPass : vk.RenderPass;
pipelineLayout : vk.PipelineLayout;
graphicsPipeline : vk.Pipeline;

commandPool : vk.CommandPool;
commandBuffers : []vk.CommandBuffer;

imageAvailableSemaphores : []vk.Semaphore;
renderFinishedSemaphores : []vk.Semaphore;
inFlightFences : []vk.Fence;
imagesInFlight : []vk.Fence;
currentFrame : u32 = 0;

initWindow :: proc() {
    err : wm.ErrorStr;
    err, window = wm.spawn_window("Vulkan", WIDTH, HEIGHT);
}

initVulkan :: proc() {
    createInstance();
    createSurface();
    pickPhysicalDevice();
    createLogicalDevice();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandPool();
    createCommandBuffers();
    createSyncObjects();
}

mainLoop :: proc() {
    for wm.handle_msgs(window) {
        drawFrame();
    }

    vk.device_wait_idle(device);
}

createInstance :: proc() {
    if(enableValidationLayers && !checkValidationLayerSupport())
    {
        panic("validation layers requested, but not available!");
        return;
    }

    appInfo : vk.ApplicationInfo = {};
    appInfo.sType = vk.StructureType.ApplicationInfo;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = vk.API_VERSION_1_1;
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = vk.API_VERSION_1_1;
    appInfo.apiVersion = vk.API_VERSION_1_1;

    createInfo : vk.InstanceCreateInfo = {};
    createInfo.sType = vk.StructureType.InstanceCreateInfo;
    createInfo.pApplicationInfo = &appInfo;

    extensions := getRequiredExtensions();
    createInfo.enabledExtensionCount = u32(len(extensions));
    createInfo.ppEnabledExtensionNames = mem.raw_data(extensions);

    if enableValidationLayers {
        createInfo.enabledLayerCount = u32(len(validationLayers));
        createInfo.ppEnabledLayerNames = mem.raw_data(validationLayers);

        //populateDebugMessengerCreateInfo(debugCreateInfo);
        //createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugCreateInfo;
    } else {
        createInfo.enabledLayerCount = 0;
        createInfo.pNext = nil;
    }

    if (vk.create_instance(&createInfo, nil, &instance) != vk.Result.Success) {
        panic("failed to create instance!");
        return;
    }
}

createSurface :: proc() {
    createInfo : vk.Win32SurfaceCreateInfoKHR = {};
    createInfo.sType = vk.StructureType.Win32SurfaceCreateInfoKhr;
    createInfo.hwnd = window.hwnd;
    createInfo.hinstance = window.hInstance;

    if result := vk.create_win32_surface_khr(instance, &createInfo, nil, &surface); result != vk.Result.Success {
        panic("failed to create window surface!");
    }
}

pickPhysicalDevice :: proc() {
    deviceCount : u32 = 0;
    vk.enumerate_physical_devices(instance, &deviceCount, nil);
    if deviceCount == 0 {
        panic("failed to find any GPU");
        return;
    }
    devices := make([]vk.PhysicalDevice, deviceCount);
    defer delete(devices);
    vk.enumerate_physical_devices(instance, &deviceCount, mem.raw_data(devices));

    for device in devices {
        if isDeviceSuitable(device) {
            physicalDevice = device;
            break;
        }
    }

    if physicalDevice == nil {
        panic("failed to find a suitable GPU!");
        return;
    }
}

unique :: proc(array: []$E) -> []E {
    output : [dynamic]E;
    skip: for elem in array {
        for checkElem in output {
            if elem == checkElem do continue skip;
        }
        append(&output, elem);
    }
    return output[:];
}

createLogicalDevice :: proc() {
    indices : QueueFamilyIndices = findQueueFamilies(physicalDevice);

    queueCreateInfos : [dynamic]vk.DeviceQueueCreateInfo;

    uniqueQueueFamilies := unique([]u32{indices.graphicsFamily, indices.presentFamily});

    queuePriority : f32 = 1;
    for queueFamily in uniqueQueueFamilies {
        queueCreateInfo : vk.DeviceQueueCreateInfo = {};
        queueCreateInfo.sType = vk.StructureType.DeviceQueueCreateInfo;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        append_elem(&queueCreateInfos, queueCreateInfo);
    }

    deviceFeatures : vk.PhysicalDeviceFeatures = {};

    createInfo : vk.DeviceCreateInfo = {};
    createInfo.sType = vk.StructureType.DeviceCreateInfo;

    createInfo.queueCreateInfoCount = u32(len(queueCreateInfos));
    createInfo.pQueueCreateInfos = mem.raw_data(queueCreateInfos);

    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = u32(len(deviceExtensions));
    createInfo.ppEnabledExtensionNames = mem.raw_data(deviceExtensions);

    if (enableValidationLayers) {
        createInfo.enabledLayerCount = u32(len(validationLayers));
        createInfo.ppEnabledLayerNames = mem.raw_data(validationLayers);
    } else {
        createInfo.enabledLayerCount = 0;
    }

    if (vk.create_device(physicalDevice, &createInfo, nil, &device) != vk.Result.Success) {
        panic("failed to create logical device!");
    }

    vk.get_device_queue(device, indices.graphicsFamily, 0, &graphicsQueue);
    vk.get_device_queue(device, indices.presentFamily, 0, &presentQueue);
}

createSwapChain :: proc() {
    swapChainSupport : SwapChainSupportDetails = querySwapChainSupport(physicalDevice);

    surfaceFormat : vk.SurfaceFormatKHR = chooseSwapSurfaceFormat(swapChainSupport.formats);
    presentMode : vk.PresentModeKHR = chooseSwapPresentMode(swapChainSupport.presentModes);
    extent : vk.Extent2D = chooseSwapExtent(swapChainSupport.capabilities);

    imageCount : u32 = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    createInfo : vk.SwapchainCreateInfoKHR = {};
    createInfo.sType = .SwapchainCreateInfoKhr;
    createInfo.surface = surface;

    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = { vk.ImageUsageFlags.ColorAttachment };

    indices : QueueFamilyIndices = findQueueFamilies(physicalDevice);
    queueFamilyIndices := []u32{indices.graphicsFamily, indices.presentFamily};

    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = .Concurrent;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = mem.raw_data(queueFamilyIndices);
    } else {
        createInfo.imageSharingMode = .Exclusive;
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = vk.CompositeAlphaFlagsKHR.Opaque;
    createInfo.presentMode = presentMode;
    createInfo.clipped = b32(true);

    createInfo.oldSwapchain = nil;

    if vk.create_swapchain_khr(device, &createInfo, nil, &swapChain) != vk.Result.Success {
        panic("failed to create swap chain!");
    }

    vk.get_swapchain_images_khr(device, swapChain, &imageCount, nil);
    swapChainImages = make(type_of(swapChainImages), imageCount);
    vk.get_swapchain_images_khr(device, swapChain, &imageCount, mem.raw_data(swapChainImages));

    swapChainImageFormat = surfaceFormat.format;
    swapChainExtent = extent;
}

createImageViews :: proc() {
    swapChainImageViews = make(type_of(swapChainImageViews), len(swapChainImages));

    for _,i in  swapChainImages {
        createInfo : vk.ImageViewCreateInfo = {};
        createInfo.sType = vk.StructureType.ImageViewCreateInfo;
        createInfo.image = swapChainImages[i];
        createInfo.viewType = vk.ImageViewType._2D;
        createInfo.format = swapChainImageFormat;
        createInfo.components.r = vk.ComponentSwizzle.Identity;
        createInfo.components.g = vk.ComponentSwizzle.Identity;
        createInfo.components.b = vk.ComponentSwizzle.Identity;
        createInfo.components.a = vk.ComponentSwizzle.Identity;
        createInfo.subresourceRange.aspectMask = { .Color };
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        if (vk.create_image_view(device, &createInfo, nil, &swapChainImageViews[i]) != vk.Result.Success) {
            panic("failed to create image views!");
        }
    }
}

createRenderPass :: proc() {
    colorAttachment : vk.AttachmentDescription = {};
    colorAttachment.format = swapChainImageFormat;
    colorAttachment.samples = { ._1 };
    colorAttachment.loadOp = .Clear;
    colorAttachment.storeOp = .Store;
    colorAttachment.stencilLoadOp = .DontCare;
    colorAttachment.stencilStoreOp = .DontCare;
    colorAttachment.initialLayout = .Undefined;
    colorAttachment.finalLayout = .PresentSrcKhr;

    colorAttachmentRef : vk.AttachmentReference = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = .ColorAttachmentOptimal;

    subpass : vk.SubpassDescription = {};
    subpass.pipelineBindPoint = .Graphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    dependency : vk.SubpassDependency = {};
    dependency.srcSubpass = vk.SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = { .ColorAttachmentOutput };
    dependency.srcAccessMask = { };
    dependency.dstStageMask = { .ColorAttachmentOutput };
    dependency.dstAccessMask = { .ColorAttachmentWrite };

    renderPassInfo : vk.RenderPassCreateInfo = {};
    renderPassInfo.sType = vk.StructureType.RenderPassCreateInfo;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vk.create_render_pass(device, &renderPassInfo, nil, &renderPass) != vk.Result.Success) {
        panic("failed to create render pass!");
    }
}

createGraphicsPipeline :: proc() {
    vertShaderCode := readFile("shaders/vert.glsl.spv");
    fragShaderCode := readFile("shaders/frag.glsl.spv");

    vertShaderModule : vk.ShaderModule = createShaderModule(vertShaderCode);
    fragShaderModule : vk.ShaderModule = createShaderModule(fragShaderCode);

    vertShaderStageInfo : vk.PipelineShaderStageCreateInfo = {};
    vertShaderStageInfo.sType = .PipelineShaderStageCreateInfo;
    vertShaderStageInfo.stage = .Vertex;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";

    fragShaderStageInfo : vk.PipelineShaderStageCreateInfo = {};
    fragShaderStageInfo.sType = .PipelineShaderStageCreateInfo;
    fragShaderStageInfo.stage = .Fragment;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    shaderStages := []vk.PipelineShaderStageCreateInfo{vertShaderStageInfo, fragShaderStageInfo};

    vertexInputInfo : vk.PipelineVertexInputStateCreateInfo = {};
    vertexInputInfo.sType = .PipelineVertexInputStateCreateInfo;
    vertexInputInfo.vertexBindingDescriptionCount = 0;
    vertexInputInfo.vertexAttributeDescriptionCount = 0;

    inputAssembly : vk.PipelineInputAssemblyStateCreateInfo = {};
    inputAssembly.sType = .PipelineInputAssemblyStateCreateInfo;
    inputAssembly.topology = .TriangleList;
    inputAssembly.primitiveRestartEnable = b32(vk.FALSE);

    viewport : vk.Viewport = {};
    viewport.x = 0.0;
    viewport.y = 0.0;
    viewport.width = f32(swapChainExtent.width);
    viewport.height = f32(swapChainExtent.height);
    viewport.minDepth = 0.0;
    viewport.maxDepth = 1.0;

    scissor : vk.Rect2D = {};
    scissor.offset = {0, 0};
    scissor.extent = swapChainExtent;

    viewportState : vk.PipelineViewportStateCreateInfo = {};
    viewportState.sType = .PipelineViewportStateCreateInfo;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    rasterizer : vk.PipelineRasterizationStateCreateInfo = {};
    rasterizer.sType = .PipelineRasterizationStateCreateInfo;
    rasterizer.depthClampEnable = vk.FALSE;
    rasterizer.rasterizerDiscardEnable = vk.FALSE;
    rasterizer.polygonMode = .Fill;
    rasterizer.lineWidth = 1.0;
    rasterizer.cullMode = .Back;
    rasterizer.frontFace = .Clockwise;
    rasterizer.depthBiasEnable = vk.FALSE;

    multisampling : vk.PipelineMultisampleStateCreateInfo = {};
    multisampling.sType = .PipelineMultisampleStateCreateInfo;
    multisampling.sampleShadingEnable = vk.FALSE;
    multisampling.rasterizationSamples = ._1;

    colorBlendAttachment : vk.PipelineColorBlendAttachmentState = {};
    colorBlendAttachment.colorWriteMask = { .R, .G, .B, .A};
    colorBlendAttachment.blendEnable = vk.FALSE;

    colorBlending : vk.PipelineColorBlendStateCreateInfo = {};
    colorBlending.sType = .PipelineColorBlendStateCreateInfo;
    colorBlending.logicOpEnable = vk.FALSE;
    colorBlending.logicOp = .Copy;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0;
    colorBlending.blendConstants[1] = 0.0;
    colorBlending.blendConstants[2] = 0.0;
    colorBlending.blendConstants[3] = 0.0;

    pipelineLayoutInfo : vk.PipelineLayoutCreateInfo = {};
    pipelineLayoutInfo.sType = .PipelineLayoutCreateInfo;
    pipelineLayoutInfo.setLayoutCount = 0;
    pipelineLayoutInfo.pushConstantRangeCount = 0;

    if (vk.create_pipeline_layout(device, &pipelineLayoutInfo, nil, &pipelineLayout) != vk.Result.Success) {
        panic("failed to create pipeline layout!");
    }

    pipelineInfo : vk.GraphicsPipelineCreateInfo = {};
    pipelineInfo.sType = .GraphicsPipelineCreateInfo;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = mem.raw_data(shaderStages);
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = renderPass;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = nil;

    if (vk.create_graphics_pipelines(device, nil, 1, &pipelineInfo, nil, &graphicsPipeline) != vk.Result.Success) {
        panic("failed to create graphics pipeline!");
    }

    vk.destroy_shader_module(device, fragShaderModule, nil);
    vk.destroy_shader_module(device, vertShaderModule, nil);
}

createFramebuffers :: proc() {
    swapChainFramebuffers = make([]vk.Framebuffer, len(swapChainImageViews));
    //swapChainFramebuffers.resize(swapChainImageViews.size());

    for i := 0; i < len(swapChainImageViews); i+=1 {
        attachments : []vk.ImageView = {
            swapChainImageViews[i]
        };

        framebufferInfo : vk.FramebufferCreateInfo = {};
        framebufferInfo.sType = .FramebufferCreateInfo;
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = mem.raw_data(attachments);
        framebufferInfo.width = swapChainExtent.width;
        framebufferInfo.height = swapChainExtent.height;
        framebufferInfo.layers = 1;

        if (vk.create_framebuffer(device, &framebufferInfo, nil, &swapChainFramebuffers[i]) != vk.Result.Success) {
            panic("failed to create framebuffer!");
        }
    }
}

createCommandPool :: proc() {
    queueFamilyIndices := findQueueFamilies(physicalDevice);

    poolInfo : vk.CommandPoolCreateInfo = {};
    poolInfo.sType = .CommandPoolCreateInfo;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;

    if (vk.create_command_pool(device, &poolInfo, nil, &commandPool) != vk.Result.Success) {
        panic("failed to create command pool!");
    }
}

createCommandBuffers :: proc() {
    commandBuffers = make([]vk.CommandBuffer, len(swapChainFramebuffers));

    allocInfo : vk.CommandBufferAllocateInfo = {};
    allocInfo.sType = .CommandBufferAllocateInfo;
    allocInfo.commandPool = commandPool;
    allocInfo.level = .Primary;
    allocInfo.commandBufferCount = u32(len(commandBuffers));

    if vk.allocate_command_buffers(device, &allocInfo, mem.raw_data(commandBuffers)) != vk.Result.Success {
        panic("failed to allocate command buffers!");
    }

    for i := 0; i < len(commandBuffers); i+=1 {
        beginInfo : vk.CommandBufferBeginInfo = {};
        beginInfo.sType = .CommandBufferBeginInfo;

        if (vk.begin_command_buffer(commandBuffers[i], &beginInfo) != vk.Result.Success) {
            panic("failed to begin recording command buffer!");
        }

        renderPassInfo : vk.RenderPassBeginInfo = {};
        renderPassInfo.sType = .RenderPassBeginInfo;
        renderPassInfo.renderPass = renderPass;
        renderPassInfo.framebuffer = swapChainFramebuffers[i];
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = swapChainExtent;

        clearColor : vk.ClearValue;
        clearColor.color.float32 = [4]f32{0.0, 0.0, 0.0, 1.0};
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = &clearColor;

        vk.cmd_begin_render_pass(commandBuffers[i], &renderPassInfo, .Inline);

        vk.cmd_bind_pipeline(commandBuffers[i], .Graphics, graphicsPipeline);

        vk.cmd_draw(commandBuffers[i], 3, 1, 0, 0);

        vk.cmd_end_render_pass(commandBuffers[i]);

        if (vk.end_command_buffer(commandBuffers[i]) != vk.Result.Success) {
            panic("failed to record command buffer!");
        }
    }
}

createSyncObjects :: proc() {
    imageAvailableSemaphores = make(type_of(imageAvailableSemaphores), MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores = make(type_of(renderFinishedSemaphores), MAX_FRAMES_IN_FLIGHT);
    inFlightFences = make(type_of(inFlightFences), MAX_FRAMES_IN_FLIGHT);
    imagesInFlight = make(type_of(imagesInFlight), len(swapChainImages));

    semaphoreInfo : vk.SemaphoreCreateInfo = {};
    semaphoreInfo.sType = .SemaphoreCreateInfo;

    fenceInfo : vk.FenceCreateInfo = {};
    fenceInfo.sType = .FenceCreateInfo;
    fenceInfo.flags = u32(vk.FenceCreateFlagBits.Signaled);

    for i : u32 = 0; i < MAX_FRAMES_IN_FLIGHT; i+=1 {
        if (vk.create_semaphore(device, &semaphoreInfo, nil, &imageAvailableSemaphores[i]) != vk.Result.Success ||
            vk.create_semaphore(device, &semaphoreInfo, nil, &renderFinishedSemaphores[i]) != vk.Result.Success ||
            vk.create_fence(device, &fenceInfo, nil, &inFlightFences[i]) != vk.Result.Success) {
            panic("failed to create synchronization objects for a frame!");
        }
    }
}

drawFrame :: proc() {
    vk.wait_for_fences(device, 1, &inFlightFences[currentFrame], vk.TRUE, bits.U64_MAX);

    imageIndex : u32;
    vk.acquire_next_image_khr(device, swapChain, bits.U64_MAX, imageAvailableSemaphores[currentFrame], nil, &imageIndex);

    if (imagesInFlight[imageIndex] != nil) {
        vk.wait_for_fences(device, 1, &imagesInFlight[imageIndex], vk.TRUE, bits.U64_MAX);
    }
    imagesInFlight[imageIndex] = inFlightFences[currentFrame];

    submitInfo : vk.SubmitInfo = {};
    submitInfo.sType = .SubmitInfo;

    waitSemaphores := []vk.Semaphore{ imageAvailableSemaphores[currentFrame] };
    waitStages := []vk.PipelineStageFlags{ { .ColorAttachmentOutput } };
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = mem.raw_data(waitSemaphores);
    submitInfo.pWaitDstStageMask = mem.raw_data(waitStages);

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

    signalSemaphores : []vk.Semaphore = {renderFinishedSemaphores[currentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = mem.raw_data(signalSemaphores);

    vk.reset_fences(device, 1, &inFlightFences[currentFrame]);

    if (vk.queue_submit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]) != vk.Result.Success) {
        panic("failed to submit draw command buffer!");
    }

    presentInfo : vk.PresentInfoKHR = {};
    presentInfo.sType = .PresentInfoKhr;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = mem.raw_data(signalSemaphores);

    swapChains : []vk.SwapchainKHR = {swapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = mem.raw_data(swapChains);

    presentInfo.pImageIndices = &imageIndex;

    vk.queue_present_khr(presentQueue, &presentInfo);

    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

createShaderModule :: proc(code : string) -> vk.ShaderModule {
    createInfo : vk.ShaderModuleCreateInfo = {};
    createInfo.sType = .ShaderModuleCreateInfo;
    createInfo.codeSize = uint(len(code));
    createInfo.pCode = cast(^u32)(mem.raw_data(code));

    shaderModule : vk.ShaderModule;
    if (vk.create_shader_module(device, &createInfo, nil, &shaderModule) != vk.Result.Success) {
        panic("failed to create shader module!");
    }

    return shaderModule;
}

chooseSwapSurfaceFormat :: proc(availableFormats : []vk.SurfaceFormatKHR) -> vk.SurfaceFormatKHR{
    for availableFormat in availableFormats {
        if (availableFormat.format == .B8G8R8A8Unorm && availableFormat.colorSpace == .SrgbNonlinear) {
            return availableFormat;
        }
    }
    return availableFormats[0];
}

chooseSwapPresentMode :: proc(availablePresentModes : []vk.PresentModeKHR) -> vk.PresentModeKHR{
    for availablePresentMode in availablePresentModes {
        if (availablePresentMode == vk.PresentModeKHR.Mailbox) {
            return availablePresentMode;
        }
    }

    return vk.PresentModeKHR.Fifo;
}

chooseSwapExtent :: proc(capabilities : vk.SurfaceCapabilitiesKHR) -> vk.Extent2D{
    if (capabilities.currentExtent.width != bits.U32_MAX) {
        return capabilities.currentExtent;
    } else {
        actualExtent : vk.Extent2D = {WIDTH, HEIGHT};

        actualExtent.width = max(capabilities.minImageExtent.width, min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = max(capabilities.minImageExtent.height, min(capabilities.maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

querySwapChainSupport :: proc(device : vk.PhysicalDevice) -> SwapChainSupportDetails {
    details : SwapChainSupportDetails;

    vk.get_physical_device_surface_capabilities_khr(device, surface, &details.capabilities);

    formatCount : u32;
    vk.get_physical_device_surface_formats_khr(device, surface, &formatCount, nil);

    if (formatCount != 0) {
        details.formats = make([]vk.SurfaceFormatKHR, formatCount);
        vk.get_physical_device_surface_formats_khr(device, surface, &formatCount, mem.raw_data(details.formats));
    }

    presentModeCount : u32;
    vk.get_physical_device_surface_present_modes_khr(device, surface, &presentModeCount, nil);

    if (presentModeCount != 0) {
        details.presentModes = make([]vk.PresentModeKHR, presentModeCount);
        vk.get_physical_device_surface_present_modes_khr(device, surface, &presentModeCount, mem.raw_data(details.presentModes));
    }

    return details;
}

isDeviceSuitable :: proc(device : vk.PhysicalDevice) -> bool {
    indices : QueueFamilyIndices = findQueueFamilies(device);

    extensionsSupported : bool = checkDeviceExtensionSupport(device);

    swapChainAdequate : bool = false;
    if extensionsSupported {
        swapChainSupport : SwapChainSupportDetails = querySwapChainSupport(device);
        swapChainAdequate = len(swapChainSupport.formats) > 0 && len(swapChainSupport.presentModes) > 0;
    }

    return QueueFamilyIndices_isComplete(&indices) && extensionsSupported && swapChainAdequate;
}

checkDeviceExtensionSupport :: proc(device : vk.PhysicalDevice) -> bool {
    extensionCount : u32;
    vk.enumerate_device_extension_properties(device, nil, &extensionCount, nil);

    availableExtensions := make([]vk.ExtensionProperties, extensionCount);
    vk.enumerate_device_extension_properties(device, nil, &extensionCount, mem.raw_data(availableExtensions));

    nextExtension: for extension in deviceExtensions {
        for _, idx in availableExtensions {
            dExtension := &availableExtensions[idx];
            if extension == transmute(cstring)(&dExtension.extensionName) {
                continue nextExtension;
            }
        }
        return false;
    }
    return true;
}

findQueueFamilies :: proc(device : vk.PhysicalDevice) -> QueueFamilyIndices {
    indices : QueueFamilyIndices;

    indices.graphicsFamily = ~cast(u32)(0);
    indices.presentFamily = ~cast(u32)(0);

    queueFamilyCount : u32 = 0;
    vk.get_physical_device_queue_family_properties(device, &queueFamilyCount, nil);

    queueFamilies := make([]vk.QueueFamilyProperties, queueFamilyCount);
    defer delete(queueFamilies);
    vk.get_physical_device_queue_family_properties(device, &queueFamilyCount, mem.raw_data(queueFamilies));

    i : u32 = 0;
    for queueFamily in queueFamilies {
        if .Graphics in queueFamily.queueFlags {
            indices.graphicsFamily = i;
        }

        presentSupport : b32 = false;
        vk.get_physical_device_surface_support_khr(device, i, surface, &presentSupport);

        if presentSupport {
            indices.presentFamily = i;
        }

        if QueueFamilyIndices_isComplete(&indices) {
            break;
        }

        i+=1;
    }

    return indices;
}

getRequiredExtensions :: proc() -> []cstring {
    return { "VK_KHR_surface", "VK_KHR_win32_surface" };
}

checkValidationLayerSupport :: proc() -> bool {
    layerCount : u32;
    vk.enumerate_instance_layer_properties(&layerCount, nil);

    availableLayers := make([]vk.LayerProperties, layerCount);
    defer delete(availableLayers);
    vk.enumerate_instance_layer_properties(&layerCount, mem.raw_data(availableLayers));

    outer: for layerName in validationLayers {
        for _, idx in availableLayers {
            layerProperties := availableLayers[idx];
            if layerName == transmute(cstring)(&layerProperties.layerName) do continue outer;
        }
        return false;
    }

    return true;
}

readFile :: proc(filename : string) -> string {
    data, success := os.read_entire_file(filename);
    if !success do panic("failed to open file!");
    return string(data);
}

main :: proc() {
    initWindow();
    initVulkan();
    mainLoop();
    //cleanup();
};
